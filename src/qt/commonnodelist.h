#ifndef COMMONNODELIST_H
#define COMMONNODELIST_H

/*
#include "commonnode.h"
#include "platformstyle.h"
#include "sync.h"
#include "util.h"

#include <QMenu>
#include <QTimer>
#include <QWidget>

#define MY_COMMONNODELIST_UPDATE_SECONDS                 60
#define COMMONNODELIST_UPDATE_SECONDS                    15
#define COMMONNODELIST_FILTER_COOLDOWN_SECONDS            3

namespace Ui {
    class CommonnodeList;
}

class ClientModel;
class WalletModel;

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE
*/

/** Commonnode Manager page widget */
/*
class CommonnodeList : public QWidget
{
    Q_OBJECT

public:
    explicit CommonnodeList(const PlatformStyle *platformStyle, QWidget *parent = 0);
    ~CommonnodeList();

    void setClientModel(ClientModel *clientModel);
    void setWalletModel(WalletModel *walletModel);
    void StartAlias(std::string strAlias);
    void StartAll(std::string strCommand = "start-all");

private:
    QMenu *contextMenu;
    int64_t nTimeFilterUpdated;
    bool fFilterUpdated;

public Q_SLOTS:
    void updateMyCommonnodeInfo(QString strAlias, QString strAddr, commonnode_info_t& infoMn);
    void updateMyNodeList(bool fForce = false);
    void updateNodeList();

Q_SIGNALS:

private:
    QTimer *timer;
    Ui::CommonnodeList *ui;
    ClientModel *clientModel;
    WalletModel *walletModel;

    // Protects tableWidgetCommonnodes
    CCriticalSection cs_cnlist;

    // Protects tableWidgetMyCommonnodes
    CCriticalSection cs_mycnlist;

    QString strCurrentFilter;

private Q_SLOTS:
    void showContextMenu(const QPoint &);
    void on_filterLineEdit_textChanged(const QString &strFilterIn);
    void on_startButton_clicked();
    void on_startAllButton_clicked();
    void on_startMissingButton_clicked();
    void on_tableWidgetMyCommonnodes_itemSelectionChanged();
    void on_UpdateButton_clicked();
};
*/
#endif // COMMONNODELIST_H
