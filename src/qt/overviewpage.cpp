// Copyright (c) 2011-2015 The Bitcoin Core developers
// Copyright (c) 2014-2017 The Dash Core developers
// Copyright (c) 2017-2018 The Pura Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "overviewpage.h"
#include "ui_overviewpage.h"

#include "bitcoinunits.h"
#include "clientmodel.h"
#include "guiconstants.h"
#include "guiutil.h"
#include "init.h"
#include "optionsmodel.h"
#include "platformstyle.h"
#include "transactionfilterproxy.h"
#include "transactiontablemodel.h"
#include "utilitydialog.h"
#include "walletmodel.h"

#include "instapay.h"
#include "privatepayconfig.h"
#include "masternodeman.h"
//#include "commonnodeman.h"
#include "masternode-sync.h"
//#include "commonnode-sync.h"
#include "masternodeconfig.h"
//#include "commonnodeconfig.h"
#include "privatepay-client.h"

#include <QAbstractItemDelegate>
#include <QPainter>
#include <QSettings>
#include <QTimer>

#define ICON_OFFSET 16
#define DECORATION_SIZE 60
#define NUM_ITEMS 6
#define NUM_ITEMS_ADV 7

#include <QJsonDocument>
#include <QJsonObject>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "rpc/server.h"
#include "rpc/client.h"
#include "rpcconsole.h"

class TxViewDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    TxViewDelegate(const PlatformStyle *platformStyle):
        QAbstractItemDelegate(), unit(BitcoinUnits::PURA),
        platformStyle(platformStyle)
    {

    }

    inline void paint(QPainter *painter, const QStyleOptionViewItem &option,
                      const QModelIndex &index ) const
    {
        painter->save();

        QIcon icon = qvariant_cast<QIcon>(index.data(TransactionTableModel::RawDecorationRole));
        QRect mainRect = option.rect;
        mainRect.moveLeft(ICON_OFFSET);
        int shift = 10;
        QRect decorationRect(mainRect.left(), mainRect.top() + shift, DECORATION_SIZE - shift * 2, DECORATION_SIZE - shift * 2);
        int xspace = decorationRect.width() + ICON_OFFSET;
        int ypad = 6;
        int halfheight = (mainRect.height() - 2*ypad)/2;
        QRect amountRect( mainRect.left() + xspace, mainRect.top()+ypad,            mainRect.width() - xspace - ICON_OFFSET * 2, halfheight);
        QRect addressRect(mainRect.left() + xspace, mainRect.top()+ypad+halfheight, mainRect.width() - xspace                  , halfheight);
        icon = platformStyle->SingleColorIcon(icon);
        icon.paint(painter, decorationRect);

        QDateTime date = index.data(TransactionTableModel::DateRole).toDateTime();
        QString address = index.data(Qt::DisplayRole).toString();
        qint64 amount = index.data(TransactionTableModel::AmountRole).toLongLong();
        bool confirmed = index.data(TransactionTableModel::ConfirmedRole).toBool();
        QVariant value = index.data(Qt::ForegroundRole);
        QColor foreground = option.palette.color(QPalette::Text);
        if(value.canConvert<QBrush>())
        {
            QBrush brush = qvariant_cast<QBrush>(value);
            foreground = brush.color();
        }

        painter->setPen(foreground);
        QRect boundingRect;
        painter->drawText(addressRect, Qt::AlignLeft|Qt::AlignVCenter, address, &boundingRect);

        if (index.data(TransactionTableModel::WatchonlyRole).toBool())
        {
            QIcon iconWatchonly = qvariant_cast<QIcon>(index.data(TransactionTableModel::WatchonlyDecorationRole));
            QRect watchonlyRect(boundingRect.right() + 5, mainRect.top()+ypad+halfheight, 16, halfheight);
            iconWatchonly.paint(painter, watchonlyRect);
        }

        if(amount < 0)
        {
            foreground = COLOR_NEGATIVE;
        }
        else if(!confirmed)
        {
            foreground = COLOR_UNCONFIRMED;
        }
        else
        {
            foreground = option.palette.color(QPalette::Text);
        }
        painter->setPen(foreground);
        QString amountText = BitcoinUnits::floorWithUnit(unit, amount, true, BitcoinUnits::separatorAlways);
        if(!confirmed)
        {
            amountText = QString("[") + amountText + QString("]");
        }
        painter->drawText(amountRect, Qt::AlignRight|Qt::AlignVCenter, amountText);

        painter->setPen(option.palette.color(QPalette::Text));
        painter->drawText(amountRect, Qt::AlignLeft|Qt::AlignVCenter, GUIUtil::dateTimeStr(date));

        painter->restore();
    }

    inline QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        return QSize(DECORATION_SIZE, DECORATION_SIZE);
    }

    int unit;
    const PlatformStyle *platformStyle;

};
#include "overviewpage.moc"

OverviewPage::OverviewPage(const PlatformStyle *platformStyle, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OverviewPage),
    clientModel(0),
    walletModel(0),
    currentBalance(-1),
    currentUnconfirmedBalance(-1),
    currentImmatureBalance(-1),
    currentWatchOnlyBalance(-1),
    currentWatchUnconfBalance(-1),
    currentWatchImmatureBalance(-1),
    txdelegate(new TxViewDelegate(platformStyle)),
    filter(0)
{
    ui->setupUi(this);
    QString theme = GUIUtil::getThemeName();

    // Recent transactions
    ui->listTransactions->setItemDelegate(txdelegate);
    ui->listTransactions->setIconSize(QSize(DECORATION_SIZE, DECORATION_SIZE));
    // Note: minimum height of listTransactions will be set later in updateAdvancedPSUI() to reflect actual settings
    ui->listTransactions->setAttribute(Qt::WA_MacShowFocusRect, false);

    connect(ui->listTransactions, SIGNAL(clicked(QModelIndex)), this, SLOT(handleTransactionClicked(QModelIndex)));

    // init "out of sync" warning labels
    ui->labelWalletStatus->setText(tr("out of sync"));
    ui->labelPrivatePaySyncStatus->setText(tr("out of sync"));
    ui->labelTransactionsStatus->setText(tr("out of sync"));

    // hide PS frame (helps to preserve saved size)
    // we'll setup and make it visible in updateAdvancedPSUI() later if we are not in litemode
    ui->framePrivatePay->setVisible(false);

    //Set Node Type Actions
    connect(ui->pushButtonMasterNode, SIGNAL(clicked()), this, SLOT(deployMasternode()));
    connect(ui->pushButtonCommonNode, SIGNAL(clicked()), this, SLOT(deployCommonnode()));

    // start with displaying the "out of sync" warnings
    showOutOfSyncWarning(true);

    // that's it for litemode
    if(fLiteMode) return;

    ui->pushButtonMasterNode->setGraphicsEffect(getShadowEffect());
    ui->pushButtonCommonNode->setGraphicsEffect(getShadowEffect());

    // Disable any PS UI for masternode or when autobackup is disabled or failed for whatever reason
    if(fMasterNode || nWalletBackups <= 0){
        lockPrivatePay();
        if (nWalletBackups <= 0) {
            ui->labelPrivatePayEnabled->setToolTip(tr("Automatic backups are disabled, no mixing available!"));
        }
    } else {
        if(!privatePayClient.fEnablePrivatePay){
            ui->togglePrivatePay->setText(tr("Start Mixing"));
        } else {
            ui->togglePrivatePay->setText(tr("Stop Mixing"));
        }
        // Disable privatePayClient builtin support for automatic backups while we are in GUI,
        // we'll handle automatic backups and user warnings in privatePayStatus()
        privatePayClient.fCreateAutoBackups = false;

        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(privatePayStatus()));
        timer->start(1000);
    }

    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(readGWResponse(QNetworkReply*)));

    // Commonnode check
    /*
    BOOST_FOREACH(CCommonnodeConfig::CCommonnodeEntry cne, commonnodeConfig.getEntries()) {
        if (!cne.getAlias().compare(0, cnprefix.length(), cnprefix)) {
            ui->pushButtonCommonNode->setEnabled(false);
            break;
        }
    }
*/
    ui->pushButtonCommonNode->setEnabled(false);

}

QGraphicsDropShadowEffect* OverviewPage::getShadowEffect() {
    QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect(this);
    effect->setColor(QColor(Qt::lightGray));
    effect->setBlurRadius(1);
    effect->setOffset(3,3);
    return effect;
}

void OverviewPage::handleTransactionClicked(const QModelIndex &index)
{
    if(filter)
        Q_EMIT transactionClicked(filter->mapToSource(index));
}

OverviewPage::~OverviewPage()
{
    if(!fLiteMode && !fMasterNode) disconnect(timer, SIGNAL(timeout()), this, SLOT(privatePayStatus()));
    delete ui;
}

void OverviewPage::setBalance(const CAmount& balance, const CAmount& unconfirmedBalance, const CAmount& immatureBalance, const CAmount& anonymizedBalance, const CAmount& watchOnlyBalance, const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance)
{
    currentBalance = balance;
    currentUnconfirmedBalance = unconfirmedBalance;
    currentImmatureBalance = immatureBalance;
    currentAnonymizedBalance = anonymizedBalance;
    currentWatchOnlyBalance = watchOnlyBalance;
    currentWatchUnconfBalance = watchUnconfBalance;
    currentWatchImmatureBalance = watchImmatureBalance;
    ui->labelBalance->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, balance, false, BitcoinUnits::separatorAlways));
    ui->labelUnconfirmed->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, unconfirmedBalance, false, BitcoinUnits::separatorAlways));
    ui->labelImmature->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, immatureBalance, false, BitcoinUnits::separatorAlways));
    ui->labelAnonymized->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, anonymizedBalance, false, BitcoinUnits::separatorAlways));
    ui->labelTotal->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, balance + unconfirmedBalance + immatureBalance, false, BitcoinUnits::separatorAlways));
    ui->labelWatchAvailable->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, watchOnlyBalance, false, BitcoinUnits::separatorAlways));
    ui->labelWatchPending->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, watchUnconfBalance, false, BitcoinUnits::separatorAlways));
    ui->labelWatchImmature->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, watchImmatureBalance, false, BitcoinUnits::separatorAlways));
    ui->labelWatchTotal->setText(BitcoinUnits::floorHtmlWithUnit(nDisplayUnit, watchOnlyBalance + watchUnconfBalance + watchImmatureBalance, false, BitcoinUnits::separatorAlways));

    // only show immature (newly mined) balance if it's non-zero, so as not to complicate things
    // for the non-mining users
    bool showImmature = immatureBalance != 0;
    bool showWatchOnlyImmature = watchImmatureBalance != 0;

    // for symmetry reasons also show immature label when the watch-only one is shown
    ui->labelImmature->setVisible(showImmature || showWatchOnlyImmature);
    ui->labelImmatureText->setVisible(showImmature || showWatchOnlyImmature);
    ui->labelWatchImmature->setVisible(showWatchOnlyImmature); // show watch-only immature balance

    updatePrivatePayProgress();

    static int cachedTxLocks = 0;

    if(cachedTxLocks != nCompleteTXLocks){
        cachedTxLocks = nCompleteTXLocks;
        ui->listTransactions->update();
    }
}

// show/hide watch-only labels
void OverviewPage::updateWatchOnlyLabels(bool showWatchOnly)
{
    ui->labelSpendable->setVisible(showWatchOnly);      // show spendable label (only when watch-only is active)
    ui->labelWatchonly->setVisible(showWatchOnly);      // show watch-only label
    ui->lineWatchBalance->setVisible(showWatchOnly);    // show watch-only balance separator line
    ui->labelWatchAvailable->setVisible(showWatchOnly); // show watch-only available balance
    ui->labelWatchPending->setVisible(showWatchOnly);   // show watch-only pending balance
    ui->labelWatchTotal->setVisible(showWatchOnly);     // show watch-only total balance

    if (!showWatchOnly){
        ui->labelWatchImmature->hide();
    }
    else{
        ui->labelBalance->setIndent(20);
        ui->labelUnconfirmed->setIndent(20);
        ui->labelImmature->setIndent(20);
        ui->labelTotal->setIndent(20);
    }
}

void OverviewPage::setClientModel(ClientModel *model)
{
    this->clientModel = model;
    if(model)
    {
        // Show warning if this is a prerelease version
        connect(model, SIGNAL(alertsChanged(QString)), this, SLOT(updateAlerts(QString)));
        updateAlerts(model->getStatusBarWarnings());
    }
}

void OverviewPage::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
    if(model && model->getOptionsModel())
    {
        // update the display unit, to not use the default ("PURA")
        updateDisplayUnit();
        // Keep up to date with wallet
        setBalance(model->getBalance(), model->getUnconfirmedBalance(), model->getImmatureBalance(), model->getAnonymizedBalance(),
                   model->getWatchBalance(), model->getWatchUnconfirmedBalance(), model->getWatchImmatureBalance());
        connect(model, SIGNAL(balanceChanged(CAmount,CAmount,CAmount,CAmount,CAmount,CAmount,CAmount)), this, SLOT(setBalance(CAmount,CAmount,CAmount,CAmount,CAmount,CAmount,CAmount)));

        connect(model->getOptionsModel(), SIGNAL(displayUnitChanged(int)), this, SLOT(updateDisplayUnit()));
        connect(model->getOptionsModel(), SIGNAL(privatePayRoundsChanged()), this, SLOT(updatePrivatePayProgress()));
        connect(model->getOptionsModel(), SIGNAL(privateSentAmountChanged()), this, SLOT(updatePrivatePayProgress()));
        connect(model->getOptionsModel(), SIGNAL(advancedPSUIChanged(bool)), this, SLOT(updateAdvancedPSUI(bool)));
        // explicitly update PS frame and transaction list to reflect actual settings
        updateAdvancedPSUI(model->getOptionsModel()->getShowAdvancedPSUI());

        connect(ui->privatePayAuto, SIGNAL(clicked()), this, SLOT(privatePayAuto()));
        connect(ui->privatePayReset, SIGNAL(clicked()), this, SLOT(privatePayReset()));
        connect(ui->privatePayInfo, SIGNAL(clicked()), this, SLOT(privatePayInfo()));
        connect(ui->togglePrivatePay, SIGNAL(clicked()), this, SLOT(togglePrivatePay()));
        updateWatchOnlyLabels(model->haveWatchOnly());
        connect(model, SIGNAL(notifyWatchonlyChanged(bool)), this, SLOT(updateWatchOnlyLabels(bool)));
    }
}

void OverviewPage::updateDisplayUnit()
{
    if(walletModel && walletModel->getOptionsModel())
    {
        nDisplayUnit = walletModel->getOptionsModel()->getDisplayUnit();
        if(currentBalance != -1)
            setBalance(currentBalance, currentUnconfirmedBalance, currentImmatureBalance, currentAnonymizedBalance,
                       currentWatchOnlyBalance, currentWatchUnconfBalance, currentWatchImmatureBalance);

        // Update txdelegate->unit with the current unit
        txdelegate->unit = nDisplayUnit;

        ui->listTransactions->update();
    }
}

void OverviewPage::updateAlerts(const QString &warnings)
{
    this->ui->labelAlerts->setVisible(!warnings.isEmpty());
    this->ui->labelAlerts->setText(warnings);

}

void OverviewPage::showOutOfSyncWarning(bool fShow)
{
    ui->labelWalletStatus->setVisible(fShow);
    ui->labelPrivatePaySyncStatus->setVisible(fShow);
    ui->labelTransactionsStatus->setVisible(fShow);
}

void OverviewPage::updatePrivatePayProgress()
{
    if(!masternodeSync.IsBlockchainSynced() || ShutdownRequested()) return;

    if(!pwalletMain) return;

    QString strAmountAndRounds;
    QString strPrivatePayAmount = BitcoinUnits::formatHtmlWithUnit(nDisplayUnit, privatePayClient.nPrivatePayAmount * COIN, false, BitcoinUnits::separatorAlways);

    if(currentBalance == 0)
    {
        ui->privatePayProgress->setValue(0);
        ui->privatePayProgress->setToolTip(tr("No inputs detected"));

        // when balance is zero just show info from settings
        strPrivatePayAmount = strPrivatePayAmount.remove(strPrivatePayAmount.indexOf("."), BitcoinUnits::decimals(nDisplayUnit) + 1);
        strAmountAndRounds = strPrivatePayAmount + " / " + tr("%n Rounds", "", privatePayClient.nPrivatePayRounds);

        ui->labelAmountRounds->setToolTip(tr("No inputs detected"));
        ui->labelAmountRounds->setText(strAmountAndRounds);
        return;
    }

    CAmount nDenominatedConfirmedBalance;
    CAmount nDenominatedUnconfirmedBalance;
    CAmount nAnonymizableBalance;
    CAmount nNormalizedAnonymizedBalance;
    float nAverageAnonymizedRounds;

    {
        nDenominatedConfirmedBalance = pwalletMain->GetDenominatedBalance();
        nDenominatedUnconfirmedBalance = pwalletMain->GetDenominatedBalance(true);
        nAnonymizableBalance = pwalletMain->GetAnonymizableBalance(false, false);
        nNormalizedAnonymizedBalance = pwalletMain->GetNormalizedAnonymizedBalance();
        nAverageAnonymizedRounds = pwalletMain->GetAverageAnonymizedRounds();
    }

    CAmount nMaxToAnonymize = nAnonymizableBalance + currentAnonymizedBalance;

    // If it's more than the anon threshold, limit to that.
    if(nMaxToAnonymize > privatePayClient.nPrivatePayAmount*COIN) nMaxToAnonymize = privatePayClient.nPrivatePayAmount*COIN;

    if(nMaxToAnonymize == 0) return;

    if(nMaxToAnonymize >= privatePayClient.nPrivatePayAmount * COIN) {
        ui->labelAmountRounds->setToolTip(tr("Found enough compatible inputs to anonymize %1")
                                          .arg(strPrivatePayAmount));
        strPrivatePayAmount = strPrivatePayAmount.remove(strPrivatePayAmount.indexOf("."), BitcoinUnits::decimals(nDisplayUnit) + 1);
        strAmountAndRounds = strPrivatePayAmount + " / " + tr("%n Rounds", "", privatePayClient.nPrivatePayRounds);
    } else {
        QString strMaxToAnonymize = BitcoinUnits::formatHtmlWithUnit(nDisplayUnit, nMaxToAnonymize, false, BitcoinUnits::separatorAlways);
        ui->labelAmountRounds->setToolTip(tr("Not enough compatible inputs to anonymize <span style='color:red;'>%1</span>,<br>"
                                             "will anonymize <span style='color:red;'>%2</span> instead")
                                          .arg(strPrivatePayAmount)
                                          .arg(strMaxToAnonymize));
        strMaxToAnonymize = strMaxToAnonymize.remove(strMaxToAnonymize.indexOf("."), BitcoinUnits::decimals(nDisplayUnit) + 1);
        strAmountAndRounds = "<span style='color:red;'>" +
                QString(BitcoinUnits::factor(nDisplayUnit) == 1 ? "" : "~") + strMaxToAnonymize +
                " / " + tr("%n Rounds", "", privatePayClient.nPrivatePayRounds) + "</span>";
    }
    ui->labelAmountRounds->setText(strAmountAndRounds);

    // calculate parts of the progress, each of them shouldn't be higher than 1
    // progress of denominating
    float denomPart = 0;
    // mixing progress of denominated balance
    float anonNormPart = 0;
    // completeness of full amount anonymization
    float anonFullPart = 0;

    CAmount denominatedBalance = nDenominatedConfirmedBalance + nDenominatedUnconfirmedBalance;
    denomPart = (float)denominatedBalance / nMaxToAnonymize;
    denomPart = denomPart > 1 ? 1 : denomPart;
    denomPart *= 100;

    anonNormPart = (float)nNormalizedAnonymizedBalance / nMaxToAnonymize;
    anonNormPart = anonNormPart > 1 ? 1 : anonNormPart;
    anonNormPart *= 100;

    anonFullPart = (float)currentAnonymizedBalance / nMaxToAnonymize;
    anonFullPart = anonFullPart > 1 ? 1 : anonFullPart;
    anonFullPart *= 100;

    // apply some weights to them ...
    float denomWeight = 1;
    float anonNormWeight = privatePayClient.nPrivatePayRounds;
    float anonFullWeight = 2;
    float fullWeight = denomWeight + anonNormWeight + anonFullWeight;
    // ... and calculate the whole progress
    float denomPartCalc = ceilf((denomPart * denomWeight / fullWeight) * 100) / 100;
    float anonNormPartCalc = ceilf((anonNormPart * anonNormWeight / fullWeight) * 100) / 100;
    float anonFullPartCalc = ceilf((anonFullPart * anonFullWeight / fullWeight) * 100) / 100;
    float progress = denomPartCalc + anonNormPartCalc + anonFullPartCalc;
    if(progress >= 100) progress = 100;

    ui->privatePayProgress->setValue(progress);

    QString strToolPip = ("<b>" + tr("Overall progress") + ": %1%</b><br/>" +
                          tr("Denominated") + ": %2%<br/>" +
                          tr("Mixed") + ": %3%<br/>" +
                          tr("Anonymized") + ": %4%<br/>" +
                          tr("Denominated inputs have %5 of %n rounds on average", "", privatePayClient.nPrivatePayRounds))
            .arg(progress).arg(denomPart).arg(anonNormPart).arg(anonFullPart)
            .arg(nAverageAnonymizedRounds);
    ui->privatePayProgress->setToolTip(strToolPip);
}

void OverviewPage::updateAdvancedPSUI(bool fShowAdvancedPSUI) {
    this->fShowAdvancedPSUI = fShowAdvancedPSUI;
    int nNumItems = (fLiteMode || !fShowAdvancedPSUI) ? NUM_ITEMS : NUM_ITEMS_ADV;
    SetupTransactionList(nNumItems);

    if (fLiteMode) return;

    ui->framePrivatePay->setVisible(true);
    ui->labelCompletitionText->setVisible(fShowAdvancedPSUI);
    ui->privatePayProgress->setVisible(fShowAdvancedPSUI);
    ui->labelSubmittedDenomText->setVisible(fShowAdvancedPSUI);
    ui->labelSubmittedDenom->setVisible(fShowAdvancedPSUI);
    ui->privatePayAuto->setVisible(fShowAdvancedPSUI);
    ui->privatePayReset->setVisible(fShowAdvancedPSUI);
    ui->privatePayInfo->setVisible(true);
    ui->labelPrivatePayLastMessage->setVisible(fShowAdvancedPSUI);
}

void OverviewPage::privatePayStatus()
{
    if(!ui->framePrivatePay->isEnabled()) return;
    if(!masternodeSync.IsBlockchainSynced() || ShutdownRequested()) return;

    static int64_t nLastDSProgressBlockTime = 0;
    int nBestHeight = clientModel->getNumBlocks();

    // We are processing more then 1 block per second, we'll just leave
    if(((nBestHeight - privatePayClient.nCachedNumBlocks) / (GetTimeMillis() - nLastDSProgressBlockTime + 1) > 1)) return;
    nLastDSProgressBlockTime = GetTimeMillis();

    QString strKeysLeftText(tr("keys left: %1").arg(pwalletMain->nKeysLeftSinceAutoBackup));
    if(pwalletMain->nKeysLeftSinceAutoBackup < PRIVATEPAY_KEYS_THRESHOLD_WARNING) {
        strKeysLeftText = "<span style='color:red;'>" + strKeysLeftText + "</span>";
    }
    ui->labelPrivatePayEnabled->setToolTip(strKeysLeftText);

    if (!privatePayClient.fEnablePrivatePay) {
        if (nBestHeight != privatePayClient.nCachedNumBlocks) {
            privatePayClient.nCachedNumBlocks = nBestHeight;
            updatePrivatePayProgress();
        }

        ui->labelPrivatePayLastMessage->setText("");
        ui->togglePrivatePay->setText(tr("Start Mixing"));

        QString strEnabled = tr("Disabled");
        // Show how many keys left in advanced PS UI mode only
        if (fShowAdvancedPSUI) strEnabled += ", " + strKeysLeftText;
        ui->labelPrivatePayEnabled->setText(strEnabled);

        return;
    }

    // Warn user that wallet is running out of keys
    // NOTE: we do NOT warn user and do NOT create autobackups if mixing is not running
    if (nWalletBackups > 0 && pwalletMain->nKeysLeftSinceAutoBackup < PRIVATEPAY_KEYS_THRESHOLD_WARNING) {
        QSettings settings;
        if(settings.value("fLowKeysWarning").toBool()) {
            QString strWarn =   tr("Very low number of keys left since last automatic backup!") + "<br><br>" +
                    tr("We are about to create a new automatic backup for you, however "
                       "<span style='color:red;'> you should always make sure you have backups "
                       "saved in some safe place</span>!") + "<br><br>" +
                    tr("Note: You turn this message off in options.");
            ui->labelPrivatePayEnabled->setToolTip(strWarn);
            LogPrintf("OverviewPage::privatePayStatus -- Very low number of keys left since last automatic backup, warning user and trying to create new backup...\n");
            QMessageBox::warning(this, tr("PrivatePay"), strWarn, QMessageBox::Ok, QMessageBox::Ok);
        } else {
            LogPrintf("OverviewPage::privatePayStatus -- Very low number of keys left since last automatic backup, skipping warning and trying to create new backup...\n");
        }

        std::string strBackupWarning;
        std::string strBackupError;
        if(!AutoBackupWallet(pwalletMain, "", strBackupWarning, strBackupError)) {
            if (!strBackupWarning.empty()) {
                // It's still more or less safe to continue but warn user anyway
                LogPrintf("OverviewPage::privatePayStatus -- WARNING! Something went wrong on automatic backup: %s\n", strBackupWarning);

                QMessageBox::warning(this, tr("PrivatePay"),
                                     tr("WARNING! Something went wrong on automatic backup") + ":<br><br>" + strBackupWarning.c_str(),
                                     QMessageBox::Ok, QMessageBox::Ok);
            }
            if (!strBackupError.empty()) {
                // Things are really broken, warn user and stop mixing immediately
                LogPrintf("OverviewPage::privatePayStatus -- ERROR! Failed to create automatic backup: %s\n", strBackupError);

                QMessageBox::warning(this, tr("PrivatePay"),
                                     tr("ERROR! Failed to create automatic backup") + ":<br><br>" + strBackupError.c_str() + "<br>" +
                                     tr("Mixing is disabled, please close your wallet and fix the issue!"),
                                     QMessageBox::Ok, QMessageBox::Ok);
            }
        }
    }

    QString strEnabled = privatePayClient.fEnablePrivatePay ? tr("Enabled") : tr("Disabled");
    // Show how many keys left in advanced PS UI mode only
    if(fShowAdvancedPSUI) strEnabled += ", " + strKeysLeftText;
    ui->labelPrivatePayEnabled->setText(strEnabled);

    if(nWalletBackups == -1) {
        // Automatic backup failed, nothing else we can do until user fixes the issue manually
        lockPrivatePay();

        QString strError =  tr("ERROR! Failed to create automatic backup") + ", " +
                tr("see debug.log for details.") + "<br><br>" +
                tr("Mixing is disabled, please close your wallet and fix the issue!");
        ui->labelPrivatePayEnabled->setToolTip(strError);

        return;
    } else if(nWalletBackups == -2) {
        // We were able to create automatic backup but keypool was not replenished because wallet is locked.
        QString strWarning = tr("WARNING! Failed to replenish keypool, please unlock your wallet to do so.");
        ui->labelPrivatePayEnabled->setToolTip(strWarning);
    }

    // check privatepay status and unlock if needed
    if(nBestHeight != privatePayClient.nCachedNumBlocks) {
        // Balance and number of transactions might have changed
        privatePayClient.nCachedNumBlocks = nBestHeight;
        updatePrivatePayProgress();
    }

    QString strStatus = QString(privatePayClient.GetStatus().c_str());

    QString s = tr("Last PrivatePay message:\n") + strStatus;

    if(s != ui->labelPrivatePayLastMessage->text())
        LogPrintf("OverviewPage::privatePayStatus -- Last PrivatePay message: %s\n", strStatus.toStdString());

    ui->labelPrivatePayLastMessage->setText(s);

    if(privatePayClient.nSessionDenom == 0){
        ui->labelSubmittedDenom->setText(tr("N/A"));
    } else {
        QString strDenom(CPrivatePay::GetDenominationsToString(privatePayClient.nSessionDenom).c_str());
        ui->labelSubmittedDenom->setText(strDenom);
    }

}


void OverviewPage::privatePayReset(){
    privatePayClient.ResetPool();

    QMessageBox::warning(this, tr("PrivatePay"),
                         tr("PrivatePay was successfully reset."),
                         QMessageBox::Ok, QMessageBox::Ok);
}

void OverviewPage::privatePayInfo(){
    HelpMessageDialog dlg(this, HelpMessageDialog::pshelp);
    dlg.exec();
}

void OverviewPage::togglePrivatePay(){
    QSettings settings;
    // Popup some information on first mixing
    QString hasMixed = settings.value("hasMixed").toString();
    if(hasMixed.isEmpty()){
        QMessageBox::information(this, tr("PrivatePay"),
                                 tr("If you don't want to see internal PrivatePay fees/transactions select \"Most Common\" as Type on the \"Transactions\" tab."),
                                 QMessageBox::Ok, QMessageBox::Ok);
        settings.setValue("hasMixed", "hasMixed");
    }
    if(!privatePayClient.fEnablePrivatePay){
        const CAmount nMinAmount = CPrivatePay::GetSmallestDenomination() + CPrivatePay::GetMaxCollateralAmount();
        if(currentBalance < nMinAmount){
            QString strMinAmount(BitcoinUnits::formatWithUnit(nDisplayUnit, nMinAmount));
            QMessageBox::warning(this, tr("PrivatePay"),
                                 tr("PrivatePay requires at least %1 to use.").arg(strMinAmount),
                                 QMessageBox::Ok, QMessageBox::Ok);
            return;
        }

        // if wallet is locked, ask for a passphrase
        if (walletModel->getEncryptionStatus() == WalletModel::Locked)
        {
            WalletModel::UnlockContext ctx(walletModel->requestUnlock(true));
            if(!ctx.isValid())
            {
                //unlock was cancelled
                privatePayClient.nCachedNumBlocks = std::numeric_limits<int>::max();
                QMessageBox::warning(this, tr("PrivatePay"),
                                     tr("Wallet is locked and user declined to unlock. Disabling PrivatePay."),
                                     QMessageBox::Ok, QMessageBox::Ok);
                LogPrint("privatepay", "OverviewPage::togglePrivatePay -- Wallet is locked and user declined to unlock. Disabling PrivatePay.\n");
                return;
            }
        }

    }

    privatePayClient.fEnablePrivatePay = !privatePayClient.fEnablePrivatePay;
    privatePayClient.nCachedNumBlocks = std::numeric_limits<int>::max();

    if(!privatePayClient.fEnablePrivatePay){
        ui->togglePrivatePay->setText(tr("Start Mixing"));
        privatePayClient.UnlockCoins();
    } else {
        ui->togglePrivatePay->setText(tr("Stop Mixing"));

        /* show privatepay configuration if client has defaults set */

        if(privatePayClient.nPrivatePayAmount == 0){
            PrivatepayConfig dlg(this);
            dlg.setModel(walletModel);
            dlg.exec();
        }

    }
}

void OverviewPage::SetupTransactionList(int nNumItems) {
    ui->listTransactions->setMinimumHeight(nNumItems * DECORATION_SIZE);

    if(walletModel && walletModel->getOptionsModel()) {
        // Set up transaction list
        filter = new TransactionFilterProxy();
        filter->setSourceModel(walletModel->getTransactionTableModel());
        filter->setLimit(nNumItems);
        filter->setDynamicSortFilter(true);
        filter->setSortRole(Qt::EditRole);
        filter->setShowInactive(false);
        filter->sort(TransactionTableModel::Status, Qt::DescendingOrder);

        ui->listTransactions->setModel(filter);
        ui->listTransactions->setModelColumn(TransactionTableModel::ToAddress);
    }
}

void OverviewPage::lockPrivatePay() {
    ui->togglePrivatePay->setText("(" + tr("Disabled") + ")");
    ui->privatePayAuto->setText("(" + tr("Disabled") + ")");
    ui->privatePayReset->setText("(" + tr("Disabled") + ")");
    ui->framePrivatePay->setEnabled(false);
    if (nWalletBackups <= 0) {
        ui->labelPrivatePayEnabled->setText("<span style='color:red;'>(" + tr("Disabled") + ")</span>");
    }
    privatePayClient.fEnablePrivatePay = false;
}

void OverviewPage::unlockPrivatePay() {
	ui->togglePrivatePay->setText(tr("Start Mixing"));
    ui->privatePayAuto->setText(tr("Try mix"));
    ui->privatePayReset->setText(tr("Reset"));
    ui->framePrivatePay->setEnabled(true);
    ui->labelPrivatePayEnabled->setText("");
}

void OverviewPage::privatePayAuto(){
    privatePayClient.DoAutomaticDenominating(*g_connman);
}

void OverviewPage::deployMasternode(){
    // Confirmation dialog
    QMessageBox::StandardButton mnConfirm;
    mnConfirm = QMessageBox::question(this, "MASTERNODE", "<table><tr><td>Do you really want to deploy MASTERNODE?</td></tr><tr><td nowrap>Please make sure you have " + QString::number(masternodeLimit) + " Puras<br>and money for transaction fee.</td></tr></table>",
                                      QMessageBox::Yes|QMessageBox::No);
    if (mnConfirm == QMessageBox::No)
        return;

    // Lock button
    ui->pushButtonMasterNode->setEnabled(false);
    ui->pushButtonMasterNode->setText("Processing...");

    // request unlock only if was locked or unlocked for mixing:
    // this way we let users unlock by walletpassphrase or by menu
    // and make many transactions while unlocking through this dialog
    // will call relock
    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();
    if (encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());
        if (!ctx.isValid()) {
            // Unlock wallet was cancelled
            resetMasternode();
            showMessage("MASTERNODE", "Can't unlock you wallet.", "Try one more time.");
            return;
        }
        setMasternode();
        return;
    }
    setMasternode();
    return;
}

void OverviewPage::deployCommonnode(){
    // Confirmation dialog
    QMessageBox::StandardButton cnConfirm;
    cnConfirm = QMessageBox::question(this, "COMMONNODE", "<table><tr><td>Do you really want to deploy COMMONNODE?</td></tr><tr><td nowrap>Please make sure you have " + QString::number(commonnodeLimit) + " Puras<br>and money for transaction fee.</td></tr></table>",
                                      QMessageBox::Yes|QMessageBox::No);
    if (cnConfirm == QMessageBox::No)
        return;

    // Lock button
    ui->pushButtonCommonNode->setEnabled(false);
    ui->pushButtonCommonNode->setText("Processing...");

    // request unlock only if was locked or unlocked for mixing:
    // this way we let users unlock by walletpassphrase or by menu
    // and make many transactions while unlocking through this dialog
    // will call relock
    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();
    if (encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());
        if (!ctx.isValid()) {
            // Unlock wallet was cancelled
            resetCommonnode();
            showMessage("COMMONNODE", "Can't unlock you wallet.", "Try one moretime.");
            return;
        }
        setCommonnode();
        return;
    }
    setCommonnode();
    return;
}

void OverviewPage::setMasternode() {
    masternode.account = request("getaccountaddress 0");
    if (masternode.account.isEmpty()) {
        resetMasternode();
        showMessage("MASTERNODE", "Unable to get user's wallet address.", "Try to do deploy masternode manualy.");
        return;
    }

    lockPrivatePay();
    try {
        masternode.transaction = request(QString("sendtoaddress %1 %2").arg(masternode.account, QString::number(masternodeLimit)));
    }
    catch (UniValue& objError)
    {
        QString reason = QString::fromStdString(find_value(objError, "message").get_str());
        resetMasternode();
        showMessage("MASTERNODE", "Unable to perform payment: " + reason, "Please make sure you have " + QString::number(masternodeLimit) + " Puras<br>and money for transaction fee.");
        unlockPrivatePay();
        return;
    }
    unlockPrivatePay();
    if (masternode.transaction.isEmpty()) {
        resetMasternode();
        showMessage("MASTERNODE", "Unable to perform payment.", "Please check your password and try again.");
        return;
    }

    masternode.key = request("masternode genkey");
    if (masternode.key.isEmpty()) {
        resetMasternode();
        showMessage("MASTERNODE", "Unable to generate masternode unique key.", "Try to do deploy masternode manualy.");
        return;
    }

    QString outputs = request("masternode outputs");
    QJsonDocument document = QJsonDocument::fromJson(QByteArray().append(outputs));
    QJsonObject root = document.object();
    masternode.output = root.value(masternode.transaction).toString();
    if (masternode.output.isEmpty()) {
        resetMasternode();
        showMessage("MASTERNODE", "Unavailable collateral transaction.", "Wait an hour, restart the wallet and try again");
        return;
    }
    if (!lockCoin(masternode.transaction, masternode.output)) {
        resetMasternode();
        showMessage("MASTERNODE", "Can't lock transaction.", "Wait an hour, restart the wallet and try again");
        return;
    }

    QString url = QString("http://%1:%2/pool/masternode/").arg(GATEWAYHOST, GATEWAYPORT);
    QString isTestNet = "false";
    std::string chain = ChainNameFromCommandLine();
    if (chain == CBaseChainParams::TESTNET)
        isTestNet = "true";

    QByteArray jsonString = QByteArray().append("{\"masternode_priv_key\":\"" + masternode.key + "\",\"collateral_output_txid\":\""+ masternode.transaction +"\",\"collateral_output_index\":" + masternode.output + ",\"testnet\":" + isTestNet + "}");
    QByteArray postDataSize = QByteArray::number(jsonString.size());

    QNetworkRequest request = QNetworkRequest(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Content-Length", postDataSize);
    networkManager->post(request, jsonString);
    LogPrintf("Masternode deployment: Created request for %s\n", masternode.key.toStdString());

    // Show alert.
    QMessageBox::question(this, "Masternode being created.", "Do Not close your wallet until prompted \"Ready to start Masternode\"", QMessageBox::Ok);
}

void OverviewPage::setCommonnode() {
    commonnode.account = request("getaccountaddress 0");
    if (commonnode.account.isEmpty()) {
        resetCommonnode();
        showMessage("COMMONNODE", "Unable to get user's wallet address.", "Try to do deploy commonnode manualy.");
        return;
    }

    lockPrivatePay();
    try {
        commonnode.transaction = request(QString("sendtoaddress %1 %2").arg(commonnode.account, QString::number(commonnodeLimit)));
    }
    catch (UniValue& objError)
    {
        QString reason = QString::fromStdString(find_value(objError, "message").get_str());
        resetCommonnode();
        showMessage("COMMONNODE", "Unable to perform payment: " + reason, "Please make sure you have " + QString::number(commonnodeLimit) + " Puras<br>and money for transaction fee.");
        unlockPrivatePay();
        return;
    }
    unlockPrivatePay();
    if (commonnode.transaction.isEmpty()) {
        resetCommonnode();
        showMessage("COMMONNODE", "Unable to perform payment.", "Please check your password and try again.");
        return;
    }

    commonnode.key = request("commonnode genkey");
    if (commonnode.key.isEmpty()) {
        resetCommonnode();
        showMessage("COMMONNODE", "Unable to generate commonnode unique key.", "Try to do deploy commonnode manualy.");
        return;
    }

    QString outputs = request("commonnode outputs");
    QJsonDocument document = QJsonDocument::fromJson(QByteArray().append(outputs));
    QJsonObject root = document.object();
    commonnode.output = root.value(commonnode.transaction).toString();
    if (commonnode.output.isEmpty()) {
        resetCommonnode();
        showMessage("COMMONNODE", "Unavailable collateral transaction.", "Wait an hour, restart the wallet and try again");
        return;
    }
    if (!lockCoin(commonnode.transaction, commonnode.output)) {
        resetCommonnode();
        showMessage("COMMONNODE", "Can't lock transaction.", "Wait an hour, restart the wallet and try again");
        return;
    }

    commitCommonnode();
}

void OverviewPage::resetMasternode(bool success) {
    if (!success && !masternode.transaction.isEmpty() && !masternode.output.isEmpty())
        unlockCoin(masternode.transaction, masternode.output);

    // reset masternode
    masternode.account = QString();
    masternode.key = QString();
    masternode.transaction = QString();

    // enable buttons
    ui->pushButtonMasterNode->setEnabled(true);
    ui->pushButtonMasterNode->setText("Masternode");
}

void OverviewPage::resetCommonnode(bool success) {
    if (!success && !commonnode.transaction.isEmpty() && !commonnode.output.isEmpty())
        unlockCoin(commonnode.transaction, commonnode.output);

    // reset commonnode
    commonnode.account = QString();
    commonnode.key = QString();
    commonnode.transaction = QString();

    // enable buttons
    ui->pushButtonCommonNode->setEnabled(true);
    ui->pushButtonCommonNode->setText("Commonnode");
}

void OverviewPage::showMessage(const QString& head, const QString& str1, const QString& str2) const {
    QMessageBox msgBox;
    msgBox.setText(head);
    msgBox.setInformativeText("<table><tr><td>" + str1 + "</td></tr><tr><td nowrap>" + str2 + "</td></tr></table>");
    msgBox.exec();

    LogPrintf("%s -- %s, %s\n", head.toStdString(), str1.toStdString(), str2.toStdString());
}

void OverviewPage::readGWResponse(QNetworkReply *reply) {
    LogPrintf("Masternode deployment: parse GW reply, request for %s\n", masternode.key.toStdString());
    if (reply->error() == QNetworkReply::NoError) {
        QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
        //LogPrintf("Masternode deployment: parse GW reply %s\n", document.toJson(QJsonDocument::Compact).toStdString());
        QJsonObject root = document.object();
        QString status = root.value("machine_status").toString();
        if (status == "RUN"){
            ui->pushButtonMasterNode->setText("Synchronizing...");

            QJsonObject machine = root.value("machine").toObject();
            bool sync = root.value("container_blockchain_synced").toBool();
            if (!machine.isEmpty() && sync) {
                masternode.ip = machine.value("ip").toString();
                masternode.alias = machine.value("name").toString();
                commitMasternode();

                reply->deleteLater();
                return;
            }
        }
        QTimer::singleShot(GATEWAYDELAY, this, SLOT(poolGateway()));
    } else
        LogPrintf("Masternode deployment: incorrect reply, request for %s - %s\n", masternode.key.toStdString(), reply->errorString().toStdString());

    reply->deleteLater();
}

void OverviewPage::poolGateway() {
    QString url = QString("http://%1:%2/pool/masternode/").arg(GATEWAYHOST, GATEWAYPORT) + masternode.key;
    LogPrintf("Masternode deployment: pool GW, request for %s\n", masternode.key.toStdString());
    networkManager->get(QNetworkRequest(QUrl(url)));
}

void OverviewPage::commitMasternode() {
    std::string chain = ChainNameFromCommandLine();
    if (chain == CBaseChainParams::TESTNET)
        masternode.ip += ":44443";
    else
        masternode.ip += ":44444";

    LogPrintf("Add new masternode in list");
    masternodeConfig.add(masternode.alias.toStdString(), masternode.ip.toStdString(), masternode.key.toStdString(), masternode.transaction.toStdString(), masternode.output.toStdString());

    // parse masternode.conf
    LogPrintf("Storing masternode config file %s\n", GetMasternodeConfigFile().string());
    std::string strErr;
    if(!masternodeConfig.write(strErr)) {
        resetMasternode();
		showMessage("MASTERNODE", "Can't write masternodes list: " + QString::fromStdString(strErr), "Please check masternode.conf file and try to restart your wallet.");
        return;
    }

    // Show masternode tab
    QSettings settings;
    settings.setValue("fShowMasternodesTab", true);
    Q_EMIT showMasternodesTab();

    // Confirmation dialog
    QMessageBox::StandardButton mnConfirm;
    mnConfirm = QMessageBox::question(this, "MASTERNODE", "<table><tr><td>Your MASTERNODE is ready for work.</td></tr><tr><td nowrap>Would you like to try to run it right now?</td></tr><tr><td nowrap>Or you can do it later on masternodes tab.</td></tr></table>",
                                      QMessageBox::Yes|QMessageBox::No);
    if (mnConfirm == QMessageBox::Yes)
        Q_EMIT remoteStartAlias(masternode.alias.toStdString());

    resetMasternode(true);
}

void OverviewPage::commitCommonnode() {
    /*
    LogPrintf("Commonnode deployment: write down Commonnode params, request for %s - %s\n", commonnode.key.toStdString());
    boost::filesystem::path pathCommonnodeConfigFile = GetCommonnodeConfigFile();
    boost::filesystem::ofstream streamConfig(pathCommonnodeConfigFile, std::fstream::app);

    std::string port = "44444";
    std::string chain = ChainNameFromCommandLine();
    if (chain == CBaseChainParams::TESTNET)
        port = "44443";
    std::string strHeader = cnprefix + "_commonnode 127.0.0.1:" + port + " " + commonnode.key.toStdString() + " " + commonnode.transactionID.toStdString() + " " + commonnode.outputIndex.toStdString() + "\n";
    streamConfig << strHeader;

    // parse commonnode.conf
    std::string strErr;
    if(!commonnodeConfig.read(strErr)) {
        resetCommonnode();
        showMessage("COMMONNODE", "Can't parse commonnodes list: " + QString::fromStdString(strErr), "Please check commonnode.conf file and try to restart your wallet.");
        return;
    }
    // Show commonnode tab
    QSettings settings;
    settings.setValue("fShowCommonnodesTab", true);
    Q_EMIT showCommonnodesTab();

    resetCommonnode(true);
    showMessage("COMMONNODE", "We are ready to launch new Common node", "Please check commonnodes tab");
    return;
    */
}

bool OverviewPage::lockCoin(const QString& txHash, const QString& outputIndex) {
    // Lock output spend
    if (pwalletMain) {
        LOCK(pwalletMain->cs_wallet);
        LogPrintf("Locking coins:\n");
        uint256 mnTxHash;
        mnTxHash.SetHex(txHash.toStdString());
        COutPoint outpoint = COutPoint(mnTxHash, outputIndex.toInt());
        // don't lock non-spendable outpoint (i.e. it's already spent or it's not from this wallet at all)
        if (pwalletMain->IsMine(CTxIn(outpoint)) != ISMINE_SPENDABLE) {
            LogPrintf("  %s %s - IS NOT SPENDABLE, was not locked\n", txHash.toStdString(), outputIndex.toStdString());
            return false;
        }
        pwalletMain->LockCoin(outpoint);
        LogPrintf("  %s %s - locked successfully\n", txHash.toStdString(), outputIndex.toStdString());
        return true;
    }
    return false;
}

bool OverviewPage::unlockCoin(const QString& txHash, const QString& outputIndex) {
    // Unlock output spend
    if (pwalletMain) {
        LOCK(pwalletMain->cs_wallet);
        LogPrintf("Unlocking coins:\n");
        uint256 mnTxHash;
        mnTxHash.SetHex(txHash.toStdString());
        COutPoint outpoint = COutPoint(mnTxHash, outputIndex.toInt());
        pwalletMain->UnlockCoin(outpoint);
        LogPrintf("  %s %s - unlocked successfully\n", txHash.toStdString(), outputIndex.toStdString());
        return true;
    }
    return false;
}

QString OverviewPage::request(const QString &command)
{
    std::vector<std::string> args;
    parseCommandLine(args, command.toStdString());

    std::string strPrint = "";
    // Convert argument list to JSON objects in method-dependent way,
    // and pass it along with the method name to the dispatcher.
    UniValue result = tableRPC.execute(
                args[0],
            RPCConvertValues(args[0], std::vector<std::string>(args.begin() + 1, args.end())));
    if (result.isStr())
        strPrint = result.get_str();
    else
        strPrint = result.write(2);

    return QString::fromStdString(strPrint);
}
