#include "commonnodelist.h"
#include "ui_commonnodelist.h"

/*
#include "activecommonnode.h"
#include "clientmodel.h"
#include "init.h"
#include "guiutil.h"
#include "commonnode-sync.h"
#include "commonnodeconfig.h"
#include "commonnodeman.h"
#include "sync.h"
#include "wallet/wallet.h"
#include "walletmodel.h"

#include <QTimer>
#include <QMessageBox>

CommonnodeList::CommonnodeList(const PlatformStyle *platformStyle, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CommonnodeList),
    clientModel(0),
    walletModel(0)
{
    ui->setupUi(this);

    ui->startButton->setEnabled(false);

    int columnAliasWidth = 100;
    int columnAddressWidth = 200;
    int columnProtocolWidth = 60;
    int columnStatusWidth = 80;
    int columnActiveWidth = 130;
    int columnLastSeenWidth = 130;

    ui->tableWidgetMyCommonnodes->setColumnWidth(0, columnAliasWidth);
    ui->tableWidgetMyCommonnodes->setColumnWidth(1, columnAddressWidth);
    ui->tableWidgetMyCommonnodes->setColumnWidth(2, columnProtocolWidth);
    ui->tableWidgetMyCommonnodes->setColumnWidth(3, columnStatusWidth);
    ui->tableWidgetMyCommonnodes->setColumnWidth(4, columnActiveWidth);
    ui->tableWidgetMyCommonnodes->setColumnWidth(5, columnLastSeenWidth);

    ui->tableWidgetCommonnodes->setColumnWidth(0, columnAddressWidth);
    ui->tableWidgetCommonnodes->setColumnWidth(1, columnProtocolWidth);
    ui->tableWidgetCommonnodes->setColumnWidth(2, columnStatusWidth);
    ui->tableWidgetCommonnodes->setColumnWidth(3, columnActiveWidth);
    ui->tableWidgetCommonnodes->setColumnWidth(4, columnLastSeenWidth);

    ui->tableWidgetMyCommonnodes->setContextMenuPolicy(Qt::CustomContextMenu);

    QAction *startAliasAction = new QAction(tr("Start alias"), this);
    contextMenu = new QMenu();
    contextMenu->addAction(startAliasAction);
    connect(ui->tableWidgetMyCommonnodes, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&)));
    connect(startAliasAction, SIGNAL(triggered()), this, SLOT(on_startButton_clicked()));

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateNodeList()));
    connect(timer, SIGNAL(timeout()), this, SLOT(updateMyNodeList()));
    timer->start(1000);

    fFilterUpdated = false;
    nTimeFilterUpdated = GetTime();
    updateNodeList();
}

CommonnodeList::~CommonnodeList()
{
    delete ui;
}

void CommonnodeList::setClientModel(ClientModel *model)
{
    this->clientModel = model;
    if(model) {
        // try to update list when commonnode count changes
        connect(clientModel, SIGNAL(strCommonnodesChanged(QString)), this, SLOT(updateNodeList()));
    }
}

void CommonnodeList::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
}

void CommonnodeList::showContextMenu(const QPoint &point)
{
    QTableWidgetItem *item = ui->tableWidgetMyCommonnodes->itemAt(point);
    if(item) contextMenu->exec(QCursor::pos());
}

void CommonnodeList::StartAlias(std::string strAlias)
{
    std::string strStatusHtml;
    strStatusHtml += "<center>Alias: " + strAlias;

    BOOST_FOREACH(CCommonnodeConfig::CCommonnodeEntry cne, commonnodeConfig.getEntries()) {
        if(cne.getAlias() == strAlias) {
            std::string strError;
            CCommonnodeBroadcast cnb;

            bool fSuccess = CCommonnodeBroadcast::Create(cne.getIp(), cne.getPrivKey(), cne.getTxHash(), cne.getOutputIndex(), strError, cnb);

            if(fSuccess) {
                strStatusHtml += "<br>Successfully started commonnode.";
                cnodeman.UpdateCommonnodeList(cnb);
                cnb.Relay();
                cnodeman.NotifyCommonnodeUpdates();
            } else {
                strStatusHtml += "<br>Failed to start commonnode.<br>Error: " + strError;
            }
            break;
        }
    }
    strStatusHtml += "</center>";

    QMessageBox msg;
    msg.setText(QString::fromStdString(strStatusHtml));
    msg.exec();

    updateMyNodeList(true);
}

void CommonnodeList::StartAll(std::string strCommand)
{
    int nCountSuccessful = 0;
    int nCountFailed = 0;
    std::string strFailedHtml;

    BOOST_FOREACH(CCommonnodeConfig::CCommonnodeEntry cne, commonnodeConfig.getEntries()) {
        std::string strError;
        CCommonnodeBroadcast cnb;

        int32_t nOutputIndex = 0;
        if(!ParseInt32(cne.getOutputIndex(), &nOutputIndex)) {
            continue;
        }

        CTxIn txin = CTxIn(uint256S(cne.getTxHash()), nOutputIndex);

        if(strCommand == "start-missing" && cnodeman.Has(txin)) continue;

        bool fSuccess = CCommonnodeBroadcast::Create(cne.getIp(), cne.getPrivKey(), cne.getTxHash(), cne.getOutputIndex(), strError, cnb);

        if(fSuccess) {
            nCountSuccessful++;
            cnodeman.UpdateCommonnodeList(cnb);
            cnb.Relay();
            cnodeman.NotifyCommonnodeUpdates();
        } else {
            nCountFailed++;
            strFailedHtml += "\nFailed to start " + cne.getAlias() + ". Error: " + strError;
        }
    }
    pwalletMain->Lock();

    std::string returnObj;
    returnObj = strprintf("Successfully started %d commonnodes, failed to start %d, total %d", nCountSuccessful, nCountFailed, nCountFailed + nCountSuccessful);
    if (nCountFailed > 0) {
        returnObj += strFailedHtml;
    }

    QMessageBox msg;
    msg.setText(QString::fromStdString(returnObj));
    msg.exec();

    updateMyNodeList(true);
}

void CommonnodeList::updateMyCommonnodeInfo(QString strAlias, QString strAddr, commonnode_info_t& infoMn)
{
    bool fOldRowFound = false;
    int nNewRow = 0;

    for(int i = 0; i < ui->tableWidgetMyCommonnodes->rowCount(); i++) {
        if(ui->tableWidgetMyCommonnodes->item(i, 0)->text() == strAlias) {
            fOldRowFound = true;
            nNewRow = i;
            break;
        }
    }

    if(nNewRow == 0 && !fOldRowFound) {
        nNewRow = ui->tableWidgetMyCommonnodes->rowCount();
        ui->tableWidgetMyCommonnodes->insertRow(nNewRow);
    }

    QTableWidgetItem *aliasItem = new QTableWidgetItem(strAlias);
    QTableWidgetItem *addrItem = new QTableWidgetItem(infoMn.fInfoValid ? QString::fromStdString(infoMn.addr.ToString()) : strAddr);
    QTableWidgetItem *protocolItem = new QTableWidgetItem(QString::number(infoMn.fInfoValid ? infoMn.nProtocolVersion : -1));
    QTableWidgetItem *statusItem = new QTableWidgetItem(QString::fromStdString(infoMn.fInfoValid ? CCommonnode::StateToString(infoMn.nActiveState) : "MISSING"));
    QTableWidgetItem *activeSecondsItem = new QTableWidgetItem(QString::fromStdString(DurationToDHMS(infoMn.fInfoValid ? (infoMn.nTimeLastPing - infoMn.sigTime) : 0)));
    QTableWidgetItem *lastSeenItem = new QTableWidgetItem(QString::fromStdString(DateTimeStrFormat("%Y-%m-%d %H:%M",
                                                                                                   infoMn.fInfoValid ? infoMn.nTimeLastPing + QDateTime::currentDateTime().offsetFromUtc() : 0)));
    QTableWidgetItem *pubkeyItem = new QTableWidgetItem(QString::fromStdString(infoMn.fInfoValid ? CBitcoinAddress(infoMn.pubKeyCollateralAddress.GetID()).ToString() : ""));

    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 0, aliasItem);
    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 1, addrItem);
    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 2, protocolItem);
    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 3, statusItem);
    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 4, activeSecondsItem);
    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 5, lastSeenItem);
    ui->tableWidgetMyCommonnodes->setItem(nNewRow, 6, pubkeyItem);
}

void CommonnodeList::updateMyNodeList(bool fForce)
{
    TRY_LOCK(cs_mycnlist, fLockAcquired);
    if(!fLockAcquired) {
        return;
    }
    static int64_t nTimeMyListUpdated = 0;

    // automatically update my commonnode list only once in MY_COMMONNODELIST_UPDATE_SECONDS seconds,
    // this update still can be triggered manually at any time via button click
    int64_t nSecondsTillUpdate = nTimeMyListUpdated + MY_COMMONNODELIST_UPDATE_SECONDS - GetTime();
    ui->secondsLabel->setText(QString::number(nSecondsTillUpdate));

    if(nSecondsTillUpdate > 0 && !fForce) return;
    nTimeMyListUpdated = GetTime();

    ui->tableWidgetCommonnodes->setSortingEnabled(false);
    BOOST_FOREACH(CCommonnodeConfig::CCommonnodeEntry cne, commonnodeConfig.getEntries()) {
        int32_t nOutputIndex = 0;
        if(!ParseInt32(cne.getOutputIndex(), &nOutputIndex)) {
            continue;
        }

        CTxIn txin = CTxIn(uint256S(cne.getTxHash()), nOutputIndex);

        commonnode_info_t infoMn = cnodeman.GetCommonnodeInfo(txin);

        updateMyCommonnodeInfo(QString::fromStdString(cne.getAlias()), QString::fromStdString(cne.getIp()), infoMn);
    }
    ui->tableWidgetCommonnodes->setSortingEnabled(true);

    // reset "timer"
    ui->secondsLabel->setText("0");
}

void CommonnodeList::updateNodeList()
{
    TRY_LOCK(cs_cnlist, fLockAcquired);
    if(!fLockAcquired) {
        return;
    }

    static int64_t nTimeListUpdated = GetTime();

    // to prevent high cpu usage update only once in COMMONNODELIST_UPDATE_SECONDS seconds
    // or COMMONNODELIST_FILTER_COOLDOWN_SECONDS seconds after filter was last changed
    int64_t nSecondsToWait = fFilterUpdated
                            ? nTimeFilterUpdated - GetTime() + COMMONNODELIST_FILTER_COOLDOWN_SECONDS
                            : nTimeListUpdated - GetTime() + COMMONNODELIST_UPDATE_SECONDS;

    if(fFilterUpdated) ui->countLabel->setText(QString::fromStdString(strprintf("Please wait... %d", nSecondsToWait)));
    if(nSecondsToWait > 0) return;

    nTimeListUpdated = GetTime();
    fFilterUpdated = false;

    QString strToFilter;
    ui->countLabel->setText("Updating...");
    ui->tableWidgetCommonnodes->setSortingEnabled(false);
    ui->tableWidgetCommonnodes->clearContents();
    ui->tableWidgetCommonnodes->setRowCount(0);
    std::vector<CCommonnode> vCommonnodes = cnodeman.GetFullCommonnodeVector();

    BOOST_FOREACH(CCommonnode& cn, vCommonnodes)
    {
        // populate list
        // Address, Protocol, Status, Active Seconds, Last Seen, Pub Key
        QTableWidgetItem *addressItem = new QTableWidgetItem(QString::fromStdString(cn.addr.ToString()));
        QTableWidgetItem *protocolItem = new QTableWidgetItem(QString::number(cn.nProtocolVersion));
        QTableWidgetItem *statusItem = new QTableWidgetItem(QString::fromStdString(cn.GetStatus()));
        QTableWidgetItem *activeSecondsItem = new QTableWidgetItem(QString::fromStdString(DurationToDHMS(cn.lastPing.sigTime - cn.sigTime)));
        QTableWidgetItem *lastSeenItem = new QTableWidgetItem(QString::fromStdString(DateTimeStrFormat("%Y-%m-%d %H:%M", cn.lastPing.sigTime + QDateTime::currentDateTime().offsetFromUtc())));
        QTableWidgetItem *pubkeyItem = new QTableWidgetItem(QString::fromStdString(CBitcoinAddress(cn.pubKeyCollateralAddress.GetID()).ToString()));

        if (strCurrentFilter != "")
        {
            strToFilter =   addressItem->text() + " " +
                            protocolItem->text() + " " +
                            statusItem->text() + " " +
                            activeSecondsItem->text() + " " +
                            lastSeenItem->text() + " " +
                            pubkeyItem->text();
            if (!strToFilter.contains(strCurrentFilter)) continue;
        }

        ui->tableWidgetCommonnodes->insertRow(0);
        ui->tableWidgetCommonnodes->setItem(0, 0, addressItem);
        ui->tableWidgetCommonnodes->setItem(0, 1, protocolItem);
        ui->tableWidgetCommonnodes->setItem(0, 2, statusItem);
        ui->tableWidgetCommonnodes->setItem(0, 3, activeSecondsItem);
        ui->tableWidgetCommonnodes->setItem(0, 4, lastSeenItem);
        ui->tableWidgetCommonnodes->setItem(0, 5, pubkeyItem);
    }

    ui->countLabel->setText(QString::number(ui->tableWidgetCommonnodes->rowCount()));
    ui->tableWidgetCommonnodes->setSortingEnabled(true);
}

void CommonnodeList::on_filterLineEdit_textChanged(const QString &strFilterIn)
{
    strCurrentFilter = strFilterIn;
    nTimeFilterUpdated = GetTime();
    fFilterUpdated = true;
    ui->countLabel->setText(QString::fromStdString(strprintf("Please wait... %d", COMMONNODELIST_FILTER_COOLDOWN_SECONDS)));
}

void CommonnodeList::on_startButton_clicked()
{
    std::string strAlias;
    {
        LOCK(cs_mycnlist);
        // Find selected node alias
        QItemSelectionModel* selectionModel = ui->tableWidgetMyCommonnodes->selectionModel();
        QModelIndexList selected = selectionModel->selectedRows();

        if(selected.count() == 0) return;

        QModelIndex index = selected.at(0);
        int nSelectedRow = index.row();
        strAlias = ui->tableWidgetMyCommonnodes->item(nSelectedRow, 0)->text().toStdString();
    }

    // Display message box
    QMessageBox::StandardButton retval = QMessageBox::question(this, tr("Confirm commonnode start"),
        tr("Are you sure you want to start commonnode %1?").arg(QString::fromStdString(strAlias)),
        QMessageBox::Yes | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if(retval != QMessageBox::Yes) return;

    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();

    if(encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());

        if(!ctx.isValid()) return; // Unlock wallet was cancelled

        StartAlias(strAlias);
        return;
    }

    StartAlias(strAlias);
}

void CommonnodeList::on_startAllButton_clicked()
{
    // Display message box
    QMessageBox::StandardButton retval = QMessageBox::question(this, tr("Confirm all commonnodes start"),
        tr("Are you sure you want to start ALL commonnodes?"),
        QMessageBox::Yes | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if(retval != QMessageBox::Yes) return;

    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();

    if(encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());

        if(!ctx.isValid()) return; // Unlock wallet was cancelled

        StartAll();
        return;
    }

    StartAll();
}

void CommonnodeList::on_startMissingButton_clicked()
{

    if(!commonnodeSync.IsCommonnodeListSynced()) {
        QMessageBox::critical(this, tr("Command is not available right now"),
            tr("You can't use this command until commonnode list is synced"));
        return;
    }

    // Display message box
    QMessageBox::StandardButton retval = QMessageBox::question(this,
        tr("Confirm missing commonnodes start"),
        tr("Are you sure you want to start MISSING commonnodes?"),
        QMessageBox::Yes | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if(retval != QMessageBox::Yes) return;

    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();

    if(encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());

        if(!ctx.isValid()) return; // Unlock wallet was cancelled

        StartAll("start-missing");
        return;
    }

    StartAll("start-missing");
}

void CommonnodeList::on_tableWidgetMyCommonnodes_itemSelectionChanged()
{
    if(ui->tableWidgetMyCommonnodes->selectedItems().count() > 0) {
        ui->startButton->setEnabled(true);
    }
}

void CommonnodeList::on_UpdateButton_clicked()
{
    updateMyNodeList(true);
}
*/
