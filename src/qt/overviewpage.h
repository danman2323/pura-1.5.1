// Copyright (c) 2011-2015 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_QT_OVERVIEWPAGE_H
#define BITCOIN_QT_OVERVIEWPAGE_H

#include "amount.h"

#include <QWidget>
#include <QGraphicsDropShadowEffect>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class ClientModel;
class TransactionFilterProxy;
class TxViewDelegate;
class PlatformStyle;
class WalletModel;

namespace Ui {
class OverviewPage;
}

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/** Overview ("home") page widget */
class OverviewPage : public QWidget
{
    Q_OBJECT

public:
    explicit OverviewPage(const PlatformStyle *platformStyle, QWidget *parent = 0);
    ~OverviewPage();

    void setClientModel(ClientModel *clientModel);
    void setWalletModel(WalletModel *walletModel);
    void showOutOfSyncWarning(bool fShow);

public Q_SLOTS:
    void privatePayStatus();
    void setBalance(const CAmount& balance, const CAmount& unconfirmedBalance, const CAmount& immatureBalance, const CAmount& anonymizedBalance,
                    const CAmount& watchOnlyBalance, const CAmount& watchUnconfBalance, const CAmount& watchImmatureBalance);
    void readGWResponse(QNetworkReply *reply);

Q_SIGNALS:
    void transactionClicked(const QModelIndex &index);
    void showMasternodesTab(bool fForce = true);
    void showCommonnodesTab(bool fForce = true);
    void remoteStartAlias(std::string strAlias);

private:
    QGraphicsDropShadowEffect* getShadowEffect();
    QTimer *timer;
    Ui::OverviewPage *ui;
    ClientModel *clientModel;
    WalletModel *walletModel;
    QNetworkAccessManager *networkManager;
    CAmount currentBalance;
    CAmount currentUnconfirmedBalance;
    CAmount currentImmatureBalance;
    CAmount currentAnonymizedBalance;
    CAmount currentWatchOnlyBalance;
    CAmount currentWatchUnconfBalance;
    CAmount currentWatchImmatureBalance;
    int nDisplayUnit;
    bool fShowAdvancedPSUI;

    TxViewDelegate *txdelegate;
    TransactionFilterProxy *filter;

    void SetupTransactionList(int nNumItems);
    void lockPrivatePay();
    void unlockPrivatePay();

    struct node {
        QString key;
        QString account;
        QString transaction;
        QString output;
        QString ip;
        QString alias;
    } masternode, commonnode;

    const CAmount masternodeLimit = 100000;
    const CAmount commonnodeLimit = 1000;

    void setMasternode();
    void setCommonnode();
    void resetMasternode(bool success = false);
    void resetCommonnode(bool success = false);
    void commitMasternode();
    void commitCommonnode();
    bool lockCoin(const QString& txHash, const QString& outputIndex);
    bool unlockCoin(const QString& txHash, const QString& outputIndex);
    void showMessage(const QString& head, const QString& str1, const QString& str2) const;

    QString request(const QString& command);
private Q_SLOTS:
    void togglePrivatePay();
    void privatePayAuto();
    void privatePayReset();
    void privatePayInfo();
    void updateDisplayUnit();
    void updatePrivatePayProgress();
    void updateAdvancedPSUI(bool fShowAdvancedPSUI);
    void handleTransactionClicked(const QModelIndex &index);
    void updateAlerts(const QString &warnings);
    void updateWatchOnlyLabels(bool showWatchOnly);
    void deployMasternode();
    void deployCommonnode();
    void poolGateway();
};

#endif // BITCOIN_QT_OVERVIEWPAGE_H
